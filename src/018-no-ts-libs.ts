import _ from 'lodash';

const users = [
  {
    username: 'Edgar',
    role: 'admin'
  },
  {
    username: 'Maria',
    role: 'seller'
  },
  {
    username: 'Carlos',
    role: 'seller'
  },
  {
    username: 'Carolina',
    role: 'customer'
  },
];

const groupByRole = _.groupBy(users, (user) => user.role);
console.log(groupByRole);

const rta = 1 + '1';
