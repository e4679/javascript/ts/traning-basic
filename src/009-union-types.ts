(() => {
  let userId: string | number;
  userId = 123213;
  userId = '88834sadad'

  function greeting(text: string | number) {
    if(typeof text === 'string') {
      console.log(`es un string ${text.toUpperCase()}`);
    } else {
      console.log(`Number ${text.toFixed(1)}`);
    }
  };

  greeting('ass');
  greeting(123213);
})();
