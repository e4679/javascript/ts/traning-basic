(() => {
  // Alias en TS
  type UserID = string | number | boolean;
  type Size = 'S' | 'M' | 'L' | 'XL';
  type Data = {
    size: Size,
    xsize: Size | 'XXL'
  }

  let userId: UserID;

  const data: Data = {size: 'S' , xsize: 'XXL'};
  // Literal Types
  let shirtSize: Size;
  shirtSize = 'L';
  shirtSize = 'M';
  shirtSize = 'S';
  shirtSize = 'XL';
  shirtSize = data.size

  function greeting(userId: UserID, size: Size) {
    if(typeof userId === 'string') {
      console.log(`The userId is a string value ${userId}`)
    }
  };

  greeting('123213', 'XL');
})();
