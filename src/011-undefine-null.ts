(() => {
  // No se puede asignar null o undefined porque no esta dentro de un valor numerico o string
  // let first: number = undefined;
  // let second: string = null;

  // En este caso las variables quedan de tipo ANY
  // let first = null;
  // let second = undefined;

  // in this case the var only recibed a null or undefined value but that is not a typical case
  // and it is not recomended to use
  // let first: null = null;
  // let second: undefined = undefined;

  // In case that value of var should be null it is a option to solve this problem+
  // for example in a short time the value is undefined or null becase the componen dose not lunch
  let first: number | null = null;
  let second: string | undefined = undefined;

  function sayHello(name: string | null) {
    const hello = (name) ? `Hello ${name}` : 'Hello nobody';
    console.log(hello);
  }

  sayHello('Edgar');
  sayHello('');
  sayHello(null);
  console.log('================');

  function sayHelloV2(name: string | null) {
    const hello = `Hello ${name?.toString() || 'nodoy'}`;
    console.log(hello);
  }

  sayHelloV2('Edgar');
  sayHelloV2('');
  sayHelloV2(null);

})();
