(() => {
  const calTotal = (prices:  number[]): string =>
   prices.reduce((sum, price) => sum + price)
   .toString();

   const printTotal = (prices: number[]): void =>
   console.log(calTotal(prices));

   printTotal([1,2,3]);

})();
