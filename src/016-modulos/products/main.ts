import {addProduct, calcStock, products} from './product.service';

console.log(products);

addProduct({
  name: 'Avena',
  createdAt: new Date(),
  stock: 220,
});

addProduct({
  name: 'Manzanas',
  createdAt: new Date(),
  stock: 12,
  size: 'L'
});
console.log('============= ');

const total = calcStock();

console.log(products);
console.log('============= ');
console.log('total ', total);

