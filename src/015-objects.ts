(() => {
  type Size = 'S' | 'M' | 'L' | 'XL';
  type Product = {
    title: string,
    createdAt: Date,
    stock: number,
    size?: Size
  }

  const products: Product[] = [];

  const addProduct = (data: Product) =>
  products.push(data);

  addProduct({
    title: 'Avena',
    createdAt: new Date(),
    stock: 220,
  });

  addProduct({
    title: 'Manzanas',
    createdAt: new Date(),
    stock: 12,
    size: 'L'
  });

  console.log(...products);
})();
