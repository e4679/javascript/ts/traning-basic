(() => {
  let dynamicVar: any;
  dynamicVar = 'Hi';
  const rta = (dynamicVar as string).toLocaleLowerCase();

  dynamicVar = 12121;
  const rta2 = (<number>dynamicVar).toFixed();

  console.log(rta);
  console.log(rta2);
})();

