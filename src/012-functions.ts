(() => {
  type Size = 'S' | 'M' | 'L' | 'XL';

  const data = ['Avena', new Date(), 11, 'M'] as const;
  const data2: [string, Date, number, Size] = ['Avena', new Date(), 112, 'M'];
  const data3 = ['Maiz', new Date(), 14, 'M'] as const;
  const data4 = ['Pan', new Date(), 31] as const;

  function createProductToJSON(
    title: string,
    createdAt: Date,
    stock: number,
    size: Size
  )
  {
    return {title, createdAt, stock, size};
  }

  const createProductToJSONV2 = (
    title: string,
    createdAt: Date,
    stock: number,
    size?: Size
  ) => ({title, createdAt, stock, size});

  function createProductToJSONV1(
    title: string,
    createdAt: Date,
    stock: number,
    size: Size
  )
  {
    console.log({title, createdAt, stock, size});
  }

  function getArr(...args: string[]) {
    console.log(...args);
  }

  createProductToJSONV1(...data);
  createProductToJSONV1(...data2);
  getArr(...['Camila', 'Andrea', 'Juan']);

  const product1 = createProductToJSON('Papas', new Date(), 22, 'M');
  const product2 = createProductToJSONV2('Yogurt', new Date(), 22, 'M');

  const product3 = createProductToJSONV2(...data3);
  const product4 = createProductToJSONV2(...data4);

  console.log('===========');
  console.log(product1);
  console.log(product2);
  console.log(product3);
  console.log(product4);


})();
