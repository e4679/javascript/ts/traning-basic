import {subDays, format} from 'date-fns';

const date = new Date(1998, 1, 28); // 0 === Enero && 1 === Febrero ...

const newDate = subDays(date, 30);

const newDateFormat = format(newDate, 'yyyy/MM/dd');

console.log(newDateFormat);
